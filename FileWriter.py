import json
import codecs
import xml.etree.ElementTree as ET
import FileReader
import os
import zipfile
import tkinter.filedialog as fd


def simple_txt_writer(context):
    f = open('files/file.txt', 'w')
    print('Файл file.txt записан в директорию files')
    f.write(context)
    f.close()


# simple_txt_writer('This is simple text file')


def json_writer(surname, name, group):
    data = {'students': []}
    if os.path.isfile('files/file.json'):
        for student in FileReader.json_reader()['students']:
            data['students'].append({'surname': student['surname'], 'name': student['name'], 'group': student['group']})
    data['students'].append({'surname': surname, 'name': name, 'group': group})
    with codecs.open('files/file.json', 'w', 'utf-16') as file:
        json.dump(data, file, ensure_ascii=False )
    print('Файл file.json записан в директорию files')


#json_writer('Иванов', 'Иван', 'BSBO-01-19')


def xml_writer(surname, name, group):
    if os.path.isfile('files/file.xml'):
        tree = FileReader.xml_reader()
        student = ET.SubElement(tree.getroot(), 'student')
        msurname = ET.SubElement(student, 'surname')
        mname = ET.SubElement(student, 'name')
        mgroup = ET.SubElement(student, 'group')
        msurname.text = surname
        mname.text = name
        mgroup.text = group
        tree.write('files/file.xml', encoding='utf-16')

    else:
        data = ET.Element('students')
        student = ET.SubElement(data, 'student')
        msurname = ET.SubElement(student, 'surname')
        mname = ET.SubElement(student, 'name')
        mgroup = ET.SubElement(student, 'group')
        msurname.text = surname
        mname.text = name
        mgroup.text = group

        file = open('files/file.xml', 'w', encoding='utf-16')
        file.write(ET.tostring(data, encoding='unicode'))

#xml_writer('Сидоров', 'Роман', 'БСБО-01-19')


def choose_file():
    filetypes = (("Любой", "*"), ("Текстовый файл", "*.txt"),
                 ("Изображение", "*.jpg *.gif *.png"))
    filename = fd.askopenfilename(title="Открыть файл", initialdir="./",
                                  filetypes=filetypes)
    if filename:
        return filename
    else:
        return 'files/file.json'


def zip_writer():
    with zipfile.ZipFile('archive.zip', 'w') as myzip:
        filename = choose_file()
        myzip.write(filename.rsplit('/', 2)[1]+'/'+filename.rsplit('/', 1)[1])

#zip_writer()